var model = require('../models/user.model').userModel
var supervisor = require('../models/supervisor.model').supervisorModel
var roles = require('./roles')


//de voir la liste des étudiants qu’il supervise

var viewStudents = function(req, res){
    const id = req.params.id;
    const students = [];
    supervisor.find({_id: id}, function(err, data){
        if(!err){
            students = data.students;
        }
    })
    return res.status(300).send({
        success: 'true',
        message: `find students for ${id}`,
        students: students
    })

}
//éditer son agenda
var editNoteBook = function(req, res){
    const id = req.params.id;
    
    const token = req.headers['x-access-token']
    const notebook = {
        title: req.body.title,
        startTime: req.body.startTime,
        endTime: req.body.endTime
    }
    if(roles.ToBeSupervisor(token)){
        model.updateOne({_id: id}, { $set: { notebook: notebook}}, function(err, msg){
            if(!err){
                return res.status(200).send({
                    success: "true",
                    message: "add notebook",
                    data: msg
                })
            }
        })
    }

}


// éditer ses informations personnelles telles que son numéro de téléphone
var editNum = function(req, res){
    num = req.body.phoneNumber;
    const token = req.headers['x-access-token']
    model.updateOne({id: req.params.id}, { $set: {phoneNumber: req.body.phoneNumber}} , function(err, data){
        if(!err){
            if(roles.ToBeSupervisor(token)){ // to be an supervisor veofre using this endpiont
                return res.status(200).send({
                    success: "true",
                    message: "update phone number",
                    data: data
                })
            }
        }
    })
}
//accepter ou décliner des requêtes de rencontre de la part des étudiants quil supervise.

module.exports = {
    editNoteBook: editNoteBook,
    viewStudents: viewStudents,
    editNum: editNum

}