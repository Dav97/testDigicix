var roles = require('./roles')

// login admin
 var login = function(req, res){
    const body = {
      email: req.body.email,
      password: req.body.password    
    }
    if(body.email === undefined || body.password === undefined){
      auth.authFailed(res);
      return;
    }
    if(body){
      // get user in database //
      model.findOne({email: body.email}, function(err, user){
        if(err){
          return res.status(500).send({
            sucess: 'false',
            message: 'not found user',
            eror: err
          })
        }
        else{
          if(user === undefined || user === null ){
            return res.status((401).send({
              sucess: 'false',
              message: ' not correct user'
            }))
          }
          else{
            const hash = user.email;
            t = bcrypt.compareSync(body.password, hash);
            var payload = {
              email : user.email,
              pseudo: user.pseudo,
              roles: ['admin']
            }
            if(!t)
            {
               return res.status(200).send({
                 sucess: 'true',
                 message: ' loging sucess',
                 token:  jwt.sign(payload, (config.tokenSignOptions), config.uploadsDir.privateKEY)
               });
            }
  
          }
        }
      })
    }
  }

  var create = function(res, req){
    const token = req.headers['x-access-token']
    var hashedPassword = bcrypt.hashSync(req.body.password, 8); // hash the password input
    // verify input of forms
    if (!req.body.phoneNumber) {
        return res.status(400).send({
        success: 'false',
        message: 'phone number is required',
        });
    } else if (!req.body.password) {
        return res.status(400).send({
        success: 'false',
        message: 'lecture is required',
        });
    } else if(!req.body.pseudo){
        return res.status(400).send({
        success:'false',
        message: 'pseudo not find'
        })
    }


  // body get on forms in front 
  const user = {
    pseudo : req.body.pseudo,
    password: hashedPassword,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    lastname: req.body.lastname,
    firstname: req.body.firstname,
    supervisor: req.body.supervisor,
    students : req.body.students
  };
  // verify if it is admin
  if(roles.ToBeAdmin(token)){
    model.create(user, function(err){
        if(!err){
        return res.status(201).send({
          success: 'true',
          message: 'user added successfully',
          student: user
        });
      }else{
        return res.status(404).send({
          sucess: 'false',
          eror: err
        })
      }
    })
  }}

  var logout = function(req, res){
    const token = req.headers['x-access-token'];

    return res.status(400).send({
      sucess: 'true',
      message: 'logout sucessful to app',
      token: null
    })
  }
  module.exports ={
    login:login,
    create:create,
    logout:logout
  };
  