var nodemailer = require('nodemailer')
var model = require('../models/user.model').userModel;

var students = require('../helpers/asyncUsers')
// list of students
var  getStudents = function(req, res){
    return res.status(200).send({
        sucess: "true",
        message: "retrive students list",
        students: students
    })
}
// end list

//update his number
var updateNum = function(req, res){
    num = req.body.phoneNumber;
    model.updateOne({id: req.params.id}, { $set: {phoneNumber: req.body.phoneNumber}} , function(err, data){
        if(!err){
            return res.status(200).send({
                success: "false",
                message: "update phone number",
                data: data
            })
        }
    })
}
 

// request for make accointance
var sendmail = function(req, res){
    var smtpTransport = nodemailer.createTransport({
        service: "gmail",
        host: "smtp.gmail.com",
        secure:false,
        auth: {
            user: "",
            pass: ""
        }
    });
    /*------------------SMTP Over-----------------------------*/
    
    /*------------------Routing Started ------------------------*/
           var mailOptions={
            to : req.body.email,
            subject : req.body.subject,
            message : req.body.message
        }
        smtpTransport.sendMail(mailOptions, function(error, response){
         if(error){
            return res.status(404).send({
                success: 'false',
                message: error
            })
         }else{
            return res.status(200).send({
                success: 'true',
                message: `message sent: ${response.message}`
            })
        }
    });
    };
    
    /*--------------------Routing Over---------------------------*/

    module.exports = {
        getStudents: getStudents,
        updateNum: updateNum,
        sendmail: sendmail
    }

//