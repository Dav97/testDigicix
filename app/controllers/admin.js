var roles = require('../controllers/roles')
var supervisor = require('../models/supervisor.model').supervisorModel

//L'administrateur est capable de créer un nouvel étudiant et un nouveau superviseur.

// Etudiant
var createStudent = function(req, res, next){

        const token = req.headers['x-access-token']
        var hashedPassword = bcrypt.hashSync(req.body.password, 8); // hash the password input
        // verify input of forms
        if (!req.body.phoneNumber) {
            return res.status(400).send({
            success: 'false',
            message: 'phone number is required',
            });
        } else if (!req.body.password) {
            return res.status(400).send({
            success: 'false',
            message: 'lecture is required',
            });
        } else if(!req.body.pseudo){
            return res.status(400).send({
            success:'false',
            message: 'pseudo not find'
            })
        }
    
    
      // body get on forms in front 
      const student = {
        pseudo : req.body.pseudo,
        password: hashedPassword,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        supervisor: req.body.supervisor,
      };
      // verify if it is admin
      if(roles.ToBeAdmin(token)){
        model.create(student, function(err){
            if(!err){
            return res.status(201).send({
              success: 'true',
              message: 'student added successfully',
              student: student
            });
          }else{
            return res.status(404).send({
              sucess: 'false',
              eror: err
            })
          }
        })
    }
    next()
}

// Supervisor
var createSupervisor = function(req, res, next){
        const token = req.headers['x-access-token']
        var hashedPassword = bcrypt.hashSync(req.body.password, 8); // hash the password input
        // verify input of forms
        if (!req.body.phoneNumber) {
            return res.status(400).send({
            success: 'false',
            message: 'phone number is required',
            });
        } else if (!req.body.password) {
            return res.status(400).send({
            success: 'false',
            message: 'lecture is required',
            });
        } else if(!req.body.pseudo){
            return res.status(400).send({
            success:'false',
            message: 'pseudo not find'
            })
        }
    
    
      // body get on forms in front 
      const supervisor = {
        pseudo : req.body.pseudo,
        password: hashedPassword,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        students : req.body.students,
        notebook: req.body.notebook
      };
      // verify if it is admin
      if(roles.ToBeAdmin(token)){
        model.create(supervisor, function(err){
            if(!err){
            return res.status(201).send({
              success: 'true',
              message: 'supervisor added successfully',
              supervisor: supervisor
            });
          }else{
            return res.status(404).send({
              sucess: 'false',
              eror: err
            })
          }
        })
      }
      next();
}
//Il est aussi capable d'assigner un étudiant à un superviseur et pas plus de trois étudiants par superviseur.

var assignStudent = function(req, res, next){
    const id = req.params.id;
    const students = [];
    students = req.body.students;
    supervisor.updateOne({_id: id}, { $set: {students: students}}, function(err, data){
        if(!err){
            return res.status(200).send({
                success: 'true',
                message: `assign ${data.length} students for ${id}`,
                students: data
            })
        }
    })
    next();
}

module.exports = {
    createStudent: createStudent,
    createSupervisor: createSupervisor,
    assignStudent: assignStudent
}