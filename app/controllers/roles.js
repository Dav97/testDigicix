var jwt = require('jsonwebtoken')
var config = require('../config');

// to be an admin   
var ToBeAdmin = function(token){
    const isAdmin = false;
    jwt.verify(token, config.uploadsDir.publicKEY, function(err, dataDecode){
        if(err){
            return false
        }else{
            dataDecode.find( (admin) => admin === dataDecode.admin)  
            for(const i=0; i< dataDecode.admin.length; i++){ 
                if(dataDecode.admin[i] === "admin"){
                    isAdmin = true;
                    break;
                }else{
                    isAdmin = isAdmin;
                } 
            }
        }
    })
    return isAdmin;
}
var ToBeStudent = function(token){
    const isStudent = false;
    jwt.verify(token, config.uploadsDir.publicKEY, function(err, dataDecode){
        if(err){
            return false
        }else{
            dataDecode.find( (admin) => admin === dataDecode.admin)  
            for(const i=0; i< dataDecode.admin.length; i++){ 
                if(dataDecode.admin[i] === "student"){
                    isStudent = true;
                    break;
                }else{
                    isStudent = false; 
                } 
            }
        }
        })
    return isStudent;
}
var ToBeSupervisor = function(token){
    const isSupervisor = false;
    jwt.verify(token, config.uploadsDir.publicKEY, function(err, dataDecode){
        if(err){
            return false
        }else{
            dataDecode.find( (admin) => admin === dataDecode.admin)  
            for(const i=0; i< dataDecode.admin.length; i++){ 
                if(dataDecode.admin[i] === "supervisor"){
                    isSupervisor = true;
                    break;                   
                }else{
                    isSupervisor = false;
                } 
            }
        }
    })
    return isSupervisor;
}

module.exports = {
    ToBeAdmin: ToBeAdmin,
    ToBeStudent: ToBeStudent,
    ToBeSupervisor: ToBeSupervisor
}