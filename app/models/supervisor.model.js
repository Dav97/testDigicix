var mongoose = require('mongoose')
var schema = require('./supervisor.schema');
var supervisorSchema = schema.SupervisorSchema;

supervisorModel = mongoose.model('supervisor', supervisorSchema);

module.exports = {
    supervisorModel : supervisorModel
}