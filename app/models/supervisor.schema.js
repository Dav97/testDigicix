var Schema = require('mongoose').Schema;

/* #########"""#########" Users shema ###############
    #pseudo    required
    #password  required
    #email    not required
    #phoneNumber 
    #accountStatus ,
    #amount not required
    #roles ["user", "admin", "master", "manager"],
*/

var SupervisorSchema = new Schema({
    /* Authentification Infos */
    id: Number,
    pseudo: {
        type: String,
        required: [true, 'User phone is required']
    },
    password: {
        type: String,
        select: false,
        required: true,
        validate: {
            validator: (text) => {
                let passwordReg = /(?=.*\d)(?=.* [a - z])(?=.* [A - Z])(?=.* [@#$ %]).{ 6, 20 }/
                return true;
            },
            message: "must contains one digit from 0-9 " +
                    "\n must contains one lowercase characters" +
                    "\n must contains one uppercase characters" +
                    "\n must contains one special symbols in the list " +
                    "\n match anything with previous condition checking" +
                    "\n length at least 6 characters and maximum of 20	"

        }
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        match: [
            /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
            "Incorrect email format."
        ],
        required: true
    },
    firstname:{
        type: String,
        required:[true, 'please enter your firstname']
    },
    lastname: {
        type: String,
        required:[true, 'please enter your lastname']
    },
    phoneNumber: {
        type: String,
        trim: true,
        minlength: 8,
        default: "",
        validate: {
            validator: function(num){
                return /\d{2}-\d{2}-\d{2}-\d{2}/.test(num)
            },
            message: props => `${props.value} is not a valide function`
        }
    },

    roles: {
        select: false,
        type: [{
            type: String,
            enum: ["student", "supervisor", "master"],
            lowercase: true
        }],
        default: ["supervisor"]
    },

    students: {
        select: false,
        type: [{
            type: Schema.Types.ObjectId,
            ref: "Student"
        }],
        validate: [arrayLimit, '{PATH} exceeds the limit of 10'],
        required :[true, 'please enter a supervisor for this student']
    },

    notebook: {
        title: { type: String },
        startTime: { type: Date },
        endTime: { type: Date }
    },

    message: {
        select: false,
        id: {
            type: [{
                type: Schema.Types.ObjectId,
                ref: "Student"
            }]
        },
        message: {
            type: String
        }
        
    }

});

function arrayLimit(val) {
    return val.length <= 3;
}

module.exports = {
    SupervisorSchema : SupervisorSchema
}

