var app = require('express').Router()//
var connect = require('./app/db')
var http = require('http')
var bodyParser  = require('body-parser')
var authControllers = require('./app/controllers/authcontroller');
var auth = require('./app/controllers/auth');
var admin = require('./app/controllers/admin')
var students = require('./app/controllers/students')
var supervisor = require('./app/controllers/supervisor')
var cors = require('cors')
var roles = require('./app/controllers/roles')


require('dotenv').config();
const req = app.request;
const res = app.response;
app.use(app.route)


//app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



//app.get('/', auth.login);
/*================= login auth all uers =====================*/
app.use('/api/login',roles.ToBeAdmin)
  .route('/api/login')
  .post(auth.login);

app.use('/api/logout', roles.ToBeAdmin)
  .route('/api/logout')
  .get(auth.logout)

// ================== function of students in app ===============*/
  // list of students
app.use('/api/students/list', roles.ToBeStudent)
  .route('/api/students/list')
  .get(students.getStudents)

  //update his number
app.use('/api/students/:id', roles.ToBeStudent)
  .route('/api/students/:id')
  .put(students.updateNum)

  // request for make accointance
app.use('/api/students/', roles.ToBeStudent)
  .route('/api/students')
  .post(students.sendmail)

// ================== function of supervisor in app ===============*/

  //de voir la liste des étudiants qu’il supervise
  //éditer son agenda
   // éditer ses informations personnelles telles que son numéro de téléphone

app.use('/api/supervisor/:id', roles.ToBeSupervisor)
  .route('/api/supervisor/:id')
  .get(supervisor.viewStudents)
  .put(supervisor.editNoteBook)
  .put(supervisor.editNum)


/* ================== function of admin in app ====================*/

app.use('/api/create/', roles.ToBeAdmin)
  .route('/api/create')
  .post(admin.createStudent)
  .post(admin.createSupervisor)

app.use('/api/assign/:id', roles.ToBeSupervisor)
  .route('/api/assign/:id')
  .put(admin.assignStudent)


app.use('/api/auth', authControllers)







/*
const arr =[1, 2, 3, 4, 5,9];
const sum = arr.reduce(function(a, b){
  return a + b
}, 0);


const bigMore = arr.filter(function(a){
    return a > 2;
  });
*/  
const server = http.createServer(app) //creation du serveur node 
const PORT = 8000
server.listen(PORT, ()=>{
    console.log('connection to server');
    const con = connect.connect;
    console.log(con.then(
      console.log("connection")
    ).catch(console.error()))
    
})

module.exports = app;

/* BATA Mozaire*/